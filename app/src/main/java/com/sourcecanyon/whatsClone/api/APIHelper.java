package com.sourcecanyon.whatsClone.api;

import com.sourcecanyon.whatsClone.api.apiServices.GroupsService;
import com.sourcecanyon.whatsClone.api.apiServices.UsersContacts;
import com.sourcecanyon.whatsClone.app.WhatsCloneApplication;

/**
 * Created by Abderrahim El imame on 4/11/17.
 *
 * @Email : abderrahim.elimame@gmail.com
 * @Author : https://twitter.com/Ben__Cherif
 * @Skype : ben-_-cherif
 */

public class APIHelper {

    public static UsersContacts initialApiUsersContacts() {
        APIService mApiService = APIService.with(WhatsCloneApplication.getInstance());
        return new UsersContacts(WhatsCloneApplication.getRealmDatabaseInstance(), WhatsCloneApplication.getInstance(), mApiService);
    }


    public static GroupsService initializeApiGroups() {
        APIService mApiService = APIService.with(WhatsCloneApplication.getInstance());
        return new GroupsService(WhatsCloneApplication.getRealmDatabaseInstance(), WhatsCloneApplication.getInstance(), mApiService);
    }
}
